module Data.Swagger.Model.Util where

import Data.Aeson hiding (Array)
import Data.Aeson.Types (Pair)
import Data.Text (Text)

fromPairs :: ToJSON a => [(Text, a)] -> Value
fromPairs = object . map (\p -> fst p .= toJSON (snd p))

infixr 5 #

(#) :: Pair -> [Pair] -> [Pair]
(_, Null) # pp = pp
p         # pp = p:pp
{-# INLINE (#) #-}

